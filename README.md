# README #

This repository is a Python based case study of extra-tropical clouds through satellite and radar observations prepared for UBC undergrad course [ATSC 301 - Atmospheric Radiation and Remote Sensing](http://clouds.eos.ubc.ca/~phil/courses/atsc301/syllabus.html)

If you have any questions or suggestions, feel free to email me (I'm only a master student, I make mistakes very often). 

----- Yingkai Sha (Kyle) <yingkai@eos.ubc.ca>